import os
import glob
import re

for file in glob.glob(os.curdir + '/*'):
    match = re.search(r'(.\\)(?P<filename>.+)\.(?P<ext>\w+)', file)
    if "py" in match.group('ext'):
        continue
    newFileName = match.group('filename')[::-1] + "." + match.group('ext')
    os.rename(file, newFileName)
    